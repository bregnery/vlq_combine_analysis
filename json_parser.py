import json
import os, sys


json_700_TPrime  = json.load(open("limits_TPrimeTPrime_M-700.json"))["700.0"]
json_1000_TPrime = json.load(open("limits_TPrimeTPrime_M-1000.json"))["1000.0"]
json_1100_TPrime = json.load(open("limits_TPrimeTPrime_M-1100.json"))["1100.0"]
json_1200_TPrime = json.load(open("limits_TPrimeTPrime_M-1200.json"))["1200.0"]
json_1300_TPrime = json.load(open("limits_TPrimeTPrime_M-1300.json"))["1300.0"]
json_1400_TPrime = json.load(open("limits_TPrimeTPrime_M-1400.json"))["1400.0"]
json_1500_TPrime = json.load(open("limits_TPrimeTPrime_M-1500.json"))["1500.0"]
json_1600_TPrime = json.load(open("limits_TPrimeTPrime_M-1600.json"))["1600.0"]
json_1700_TPrime = json.load(open("limits_TPrimeTPrime_M-1700.json"))["1700.0"]
json_1800_TPrime = json.load(open("limits_TPrimeTPrime_M-1800.json"))["1800.0"]

combined_json_TPrime = {}
combined_json_TPrime["700.0"] = json_700_TPrime
combined_json_TPrime["1000.0"] = json_1000_TPrime
combined_json_TPrime["1100.0"] = json_1100_TPrime
combined_json_TPrime["1200.0"] = json_1200_TPrime
combined_json_TPrime["1300.0"] = json_1300_TPrime
combined_json_TPrime["1400.0"] = json_1400_TPrime
combined_json_TPrime["1500.0"] = json_1500_TPrime
combined_json_TPrime["1600.0"] = json_1600_TPrime
combined_json_TPrime["1700.0"] = json_1700_TPrime
combined_json_TPrime["1800.0"] = json_1800_TPrime
print(combined_json_TPrime.keys())
with open('limits_mt_TPrime.json', 'w') as fp:
    json.dump(combined_json_TPrime, fp)


json_900_BPrime  = json.load(open("limits_BPrimeBPrime_M-900.json"))["900.0"]
json_1000_BPrime = json.load(open("limits_BPrimeBPrime_M-1000.json"))["1000.0"]
json_1100_BPrime = json.load(open("limits_BPrimeBPrime_M-1100.json"))["1100.0"]
json_1200_BPrime = json.load(open("limits_BPrimeBPrime_M-1200.json"))["1200.0"]
json_1300_BPrime = json.load(open("limits_BPrimeBPrime_M-1300.json"))["1300.0"]
json_1400_BPrime = json.load(open("limits_BPrimeBPrime_M-1400.json"))["1400.0"]
json_1500_BPrime = json.load(open("limits_BPrimeBPrime_M-1500.json"))["1500.0"]
json_1600_BPrime = json.load(open("limits_BPrimeBPrime_M-1600.json"))["1600.0"]
json_1700_BPrime = json.load(open("limits_BPrimeBPrime_M-1700.json"))["1700.0"]


combined_json_BPrime = {}
combined_json_BPrime["900.0"] = json_900_BPrime
combined_json_BPrime["1000.0"] = json_1000_BPrime
combined_json_BPrime["1100.0"] = json_1100_BPrime
combined_json_BPrime["1200.0"] = json_1200_BPrime
combined_json_BPrime["1300.0"] = json_1300_BPrime
combined_json_BPrime["1400.0"] = json_1400_BPrime
combined_json_BPrime["1500.0"] = json_1500_BPrime
combined_json_BPrime["1600.0"] = json_1600_BPrime
combined_json_BPrime["1700.0"] = json_1700_BPrime

print(combined_json_BPrime.keys())
with open('limits_mt_BPrime.json', 'w') as fp:
    json.dump(combined_json_BPrime, fp)

