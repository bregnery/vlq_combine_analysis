import os, sys
import CombineHarvester.CombineTools.ch as ch

sys.append(os.getcwd())
sys.append("/uscms/home/abhd1994/MyAnalysis/CMSSW_10_2_18/src/VLQAnalysis/FinalStateAnalysis/Analysis_Utilities")
import make_histograms as mh



cb = ch.CombineHarvester()
inname = 'inputs.root'
backgrounds = ['DataDriven-Multijet', 'WJets', 'ZJets', 'ttWJets', 'ttWW' , 'ttWZ', 'ttZJets', 'ttZZ', 'ttbar']
signals = ['TPrimeTPrime_M-700']
mc = [x for x in backgrounds+signals if 'DataDriven' not in x]
channels = mh.all_region_tags
cats = {}

for i, chan in enumerate(channels):
    cb.AddObservations(['*'], ['process_name'], ['13TeV'], [chan], [i, chan])
    cb.AddProcesses(['*'], ['process_name'], ['13TeV'], [chan], backgrounds, [i, chan], signal=False)
    cb.AddProcesses(['*'], ['process_name'], ['13TeV'], [chan], signals, [i, chan], signal=True)


cb.cp().process(mc).AddSyst(cb, "lumi_$ERA_2017", "lnN", ch.SystMap()(1.025))
# Add Normalization Uncerts
cb.cp().process(mc).AddSyst(
    cb, "Wjets_xsec", 
)
