import os, sys
import ROOT


combined_analysis_base_dir_Run2017 = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment/"

combined_datacard_TPrime_700 = os.path.join(combined_analysis_base_dir_Run2017, "TPrimeTPrime_M-700/Combined_TPrimeTPrime_M-700_2017.txt")
combined_datacard_TPrime_1000 = os.path.join(combined_analysis_base_dir_Run2017, "TPrimeTPrime_M-1000/Combined_TPrimeTPrime_M-1000_2017.txt")
combined_datacard_TPrime_1100 = os.path.join(combined_analysis_base_dir_Run2017, "TPrimeTPrime_M-1100/Combined_TPrimeTPrime_M-1100_2017.txt")
combined_datacard_TPrime_1200 = os.path.join(combined_analysis_base_dir_Run2017, "TPrimeTPrime_M-1200/Combined_TPrimeTPrime_M-1200_2017.txt")
combined_datacard_TPrime_1500 = os.path.join(combined_analysis_base_dir_Run2017, "TPrimeTPrime_M-1500/Combined_TPrimeTPrime_M-1500_2017.txt")

print(combined_datacard_TPrime_1100)
print(combined_datacard_TPrime_700)
print(combined_datacard_TPrime_1000)
print(combined_datacard_TPrime_1200)
print(combined_datacard_TPrime_1500)

base_command = "combine -M FitDiagnostics --plots "
os.system(base_command+combined_datacard_TPrime_700)
os.system(base_command+combined_datacard_TPrime_1000)
os.system(base_command+combined_datacard_TPrime_1100)
os.system(base_command+combined_datacard_TPrime_1200)
os.system(base_command+combined_datacard_TPrime_1500)

