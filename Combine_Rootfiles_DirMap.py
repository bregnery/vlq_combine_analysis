import json
import os, sys
from collections import defaultdict

this_dir = os.getcwd()
sys.path.append(this_dir)
from histogram_reshapingUtils import all_region_tags


CombineRootFiles_DirMap = defaultdict(dict)
Run2016_basedir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2016_Compartment/"
Run2017_basedir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment/"
Run2018_basedir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2018_Compartment/"

CombineRootFiles_DirMap["2016"] = defaultdict(dict)
CombineRootFiles_DirMap["2017"] = defaultdict(dict)
CombineRootFiles_DirMap["2018"] = defaultdict(dict)

Run2016_signal_masspoints = os.listdir(Run2016_basedir)
Run2017_signal_masspoints = os.listdir(Run2017_basedir)
Run2018_signal_masspoints = os.listdir(Run2018_basedir)

for i, idir in enumerate(Run2016_signal_masspoints):
    Run2016_signal_masspoints[i] = os.path.join(Run2016_basedir, idir)
for i, idir in enumerate(Run2017_signal_masspoints):
    Run2017_signal_masspoints[i] = os.path.join(Run2017_basedir, idir)
for i, idir in enumerate(Run2018_signal_masspoints):
    Run2018_signal_masspoints[i] = os.path.join(Run2018_basedir, idir)


for ij in Run2017_signal_masspoints:
    files = os.listdir(ij)
    rootfiles = []
    for ifile in files:
        if ifile.startswith("2017") and ifile.endswith(".root"):
            rootfiles.append(os.path.join(ij, ifile))
    mass_point = ij.split("/")[-1]
    textfile = open("Run2017_"+mass_point+".txt", "w")
    for rootfile_path in rootfiles:
        textfile.write(rootfile_path+"\n")
    textfile.close()
