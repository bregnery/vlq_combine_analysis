import os, sys

Run2016_base_dir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2016_Compartment/"
Run2017_base_dir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment/"
Run2018_base_dir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2018_Compartment/"

TPrime_masses = ['700']#, '1000', '1100', '1200', '1300', '1400', '1500', '1600', '1700', '1800']
signal_dirs_2016_TPrime = [os.path.join(Run2016_base_dir, "TPrimeTPrime_M-"+mass) for mass in TPrime_masses]
signal_dirs_2017_TPrime = [os.path.join(Run2017_base_dir, "TPrimeTPrime_M-"+mass) for mass in TPrime_masses]
signal_dirs_2018_TPrime = [os.path.join(Run2018_base_dir, "TPrimeTPrime_M-"+mass) for mass in TPrime_masses]

BPrime_masses = ['900', '1000', '1100', '1200', '1300', '1400', '1500', '1600', '1700']
signal_dirs_2016_BPrime = [os.path.join(Run2016_base_dir, "BPrimeBPrime_M-"+mass) for mass in BPrime_masses]
signal_dirs_2017_BPrime = [os.path.join(Run2017_base_dir, "BPrimeBPrime_M-"+mass) for mass in BPrime_masses]
signal_dirs_2018_BPrime = [os.path.join(Run2018_base_dir, "BPrimeBPrime_M-"+mass) for mass in BPrime_masses]
####################### Commnads for 2017 Data #################################


for i, mass in enumerate(TPrime_masses):
    combined_datacard_path  = os.path.join(signal_dirs_2017_TPrime[i], "Combined_TPrimeTPrime_M-"+mass+"_2017.txt")
    workspace_rootfile_path = os.path.join(signal_dirs_2017_TPrime[i], "workspace_M-"+mass+".root")
    create_workspace        = "combineTool.py -M T2W -i "+combined_datacard_path+" -o "+workspace_rootfile_path+ " -m "+mass+" --parallel 4"#--X-no-check-norm"
    os.system(create_workspace)
    generate_toys = "combineTool.py -M GenerateOnly --saveToys -t 500 "+combined_datacard_path+" -m "+mass+" -v -1 --bypassFrequentistFit"
    
    calculate_HybridNewLimit  = "combineTool.py -M HybridNew -d "+combined_datacard_path+" -m "+mass+" --LHCmode LHC-limits --singlePoint 1.0  --saveHybridResult --clsAcc 0 --toysFile=higgsCombine.Test.GenerateOnly.mH"+mass+".123456.root"
    #os.system(generate_toys)
    
    #os.system(calculate_HybridNewLimit)
    calculate_HybridNewLimit_OnCrab = "combineTool.py -d "+ workspace_rootfile_path +" -M HybridNew --LHCmode LHC-limits -T 500 -s -1 --singlePoint 0.05:1.35:0.01 --saveToys --saveHybridResult -m "+mass+" --bypassFrequentistFit --job-mode crab3 --task-name grid-test --custom-crab custom_crab.py"
    os.system(calculate_HybridNewLimit_OnCrab)
    
    
    
    
    make_impact_json        = "combineTool.py -M Impacts -d "+workspace_rootfile_path+" -m "+mass+" -o impacts_"+mass+".json"
    #os.system(make_impact_json)
    limit_root_file         = os.path.join(signal_dirs_2017_TPrime[i],"higgsCombine.limit.AsymptoticLimits.mH"+mass+".root")
    collect_output          = "combineTool.py -M CollectLimits "+limit_root_file+" --use-dirs -o limits.json"
    #os.system(collect_output)


"""
for i, mass in enumerate(BPrime_masses):
    combined_datacard_path = os.path.join(signal_dirs_2017_BPrime[i], "Combined_BPrimeBPrime_M-"+mass+"_2017.txt")
    workspace_rootfile_path = os.path.join(signal_dirs_2017_BPrime[i], "workspace_M-"+mass+".root")
    create_workspace = "combineTool.py -M T2W -i "+combined_datacard_path+" -o "+workspace_rootfile_path+ " -m "+mass+" --parallel 4"
    os.system(create_workspace)
    calculate_AsymptoticLimit = "combineTool.py -M AsymptoticLimits --run blind -d "+workspace_rootfile_path+" -m "+mass+" --there -n .limit --parallel 4"
    os.system(calculate_AsymptoticLimit)
    initial_fit_impact = "combineTool.py -M Impacts -d "+workspace_rootfile_path+" -m "+mass+" --doInitialFit --robustFit 1"
    robust_fit_impact = "combineTool.py -M Impacts -d "+workspace_rootfile_path+" -m "+mass+" --robustFit 1 --doFits"
    #os.system(initial_fit_impact)
    os.system(robust_fit_impact)
    make_impact_json = "combineTool.py -M Impacts -d "+workspace_rootfile_path+" -m "+mass+" -o impacts_"+mass+".json"
    os.system(make_impact_json)
    limit_root_file = os.path.join(signal_dirs_2017_BPrime[i],"higgsCombine.limit.AsymptoticLimits.mH"+mass+".root")
    collect_output = "combineTool.py -M CollectLimits "+limit_root_file+" --use-dirs -o limits.json"
    os.system(collect_output)

"""
