import ROOT as root
import os, sys
from array import array
from collections import defaultdict
from itertools import combinations_with_replacement, permutations

jet_labels = ["QCD", "Higgs", "top", "W", "Z", "B"]
jet_comb_m4 = list(combinations_with_replacement(jet_labels, 4))
signal_catalog = defaultdict(list)

SR_EventCategoriesLUT = {}
def assign_binary_symbol(category_list):
    q_counts = [tup.count("QCD") for tup in category_list]
    h_counts = [tup.count("Higgs") for tup in category_list]
    t_counts = [tup.count("top") for tup in category_list]
    w_counts = [tup.count("W") for tup in category_list]
    z_counts = [tup.count("Z") for tup in category_list]
    b_counts = [tup.count("B") for tup in category_list]
    encoded_bits = [0 for i in range(len(q_counts))]
    
    encoded_q = [q << 15 for q in q_counts]
    encoded_h = [h << 12 for h in h_counts]
    encoded_t = [t << 9 for t in t_counts]
    encoded_w = [w << 6 for w in w_counts]
    encoded_z = [z << 3 for z in z_counts]
    encoded_b = b_counts
    encoded_bits = [(encoded_bits[i] | encoded_q[i] | encoded_h[i] | encoded_t[i] | encoded_w[i] | encoded_z[i] | encoded_b[i]) for i in range(len(encoded_bits))]
    bit_to_str = [bin(bit) for bit in encoded_bits]
    for i in range(len(category_list)):
        SR_EventCategoriesLUT[bit_to_str[i]] = (i,category_list[i])

assign_binary_symbol(jet_comb_m4)
SR_event_cats_enc = list(SR_EventCategoriesLUT.keys())
SR_event_cats = [SR_EventCategoriesLUT[icat][1] for icat in SR_event_cats_enc]
categories = [item[1] for item in SR_EventCategoriesLUT.values()]
def get_region_tag(categories, event_tag):
    top_count = str(event_tag.count('top'))
    higgs_count = str(event_tag.count('Higgs'))
    w_count = str(event_tag.count('W'))
    z_count = str(event_tag.count('Z'))
    b_count = str(event_tag.count('B'))
    qcd_count = str(event_tag.count('QCD'))
    region_tag = top_count+"t"+w_count+"W"+z_count+"Z"+higgs_count+"H"+b_count+"b"
    return region_tag

all_region_tags = set()
for icat in categories:
    all_region_tags.add(get_region_tag(categories, icat))
###### Channel Labels ############
all_region_tags = list(all_region_tags)


def fix_negative_norm(hist):
    nonzero_entry_condition = hist.GetEntries() > 0
    if nonzero_entry_condition == True:
        for ibin in range(hist.GetNbinsX()+1):
            if hist.GetBinContent(ibin) < 0:
                print(hist.GetName(), ibin)
                hist.SetBinContent(ibin, 0.000001)
                
    return hist

def Rebin(rootfile_path):
    assert os.path.exists(rootfile_path)
    print("========= rebinning histograms in "+rootfile_path+" ============")
    rootfile = root.TFile.Open(rootfile_path, "UPDATE")
    rootfile.cd()
    #list_of_keys = list(rootfile.GetListOfKeys())
    hists_dict = {}
    individual_bkgs_keys = ["DataDriven-Multijet", "WJets", "ZJets", "ttWJets",
                       "ttWW", "ttWZ", "ttZJets", "ttZZ", "ttbar"]
    sum_of_bkgs = rootfile.GetKey("DataDriven-Multijet").ReadObj().Clone()
    nBinsOld = sum_of_bkgs.GetNbinsX()
    for ibin in range(nBinsOld+1):
        sum_of_bkgs.SetBinContent(ibin, 0.0)
        sum_of_bkgs.SetBinError(ibin, 0)
    sum_of_bkgs.SetName("SumOfBkgs")
    sum_of_bkgs.SetTitle("SumOfBkgs")
    sum_of_bkgs.Sumw2()
    for key in individual_bkgs_keys:
        hists_dict[key] = rootfile.GetKey(key).ReadObj().Clone()
        sum_of_bkgs.Add(sum_of_bkgs, hists_dict[key], 1, 1)
    SumOld = 0
    Entry = 0
    BinError = 0
    EdgesFromOld = []
    for ibin in range(nBinsOld, 0, -1):
        Entry = sum_of_bkgs.GetBinContent(ibin)
        SumOld += Entry
        BinError = sum_of_bkgs.GetBinError(ibin)
        if SumOld > 0:
            if (BinError/SumOld < 0.3):
                EdgesFromOld.append(sum_of_bkgs.GetBinLowEdge(ibin+1))
                SumOld = 0
        
    NBins = len(EdgesFromOld)
    EdgesFromOld.reverse()
    EdgesFromOld.append(0)
    EdgesFromOld[NBins] = sum_of_bkgs.GetBinLowEdge(nBinsOld+1)
    BinEdges = array("d", EdgesFromOld)
    print(BinEdges)
    NewBinned = sum_of_bkgs.Rebin(NBins, "Rebinned_"+sum_of_bkgs.GetName(), BinEdges)
    list_of_rootfile_keys = list(rootfile.GetListOfKeys())
    original_hists_dict = {}
    rebinned_hists_dict = {}
    for i, ikey in enumerate(list_of_rootfile_keys):
        key_name = ikey.GetName()
        #print(key_name, i)
        original_hists_dict[key_name] = rootfile.GetKey(key_name).ReadObj()
        hist_name = original_hists_dict[key_name].GetName()
        rebinned_hists_dict[key_name] = original_hists_dict[key_name].Rebin(NBins, hist_name, BinEdges)
        rebinned_hists_dict[key_name] = fix_negative_norm(rebinned_hists_dict[key_name])
        rebinned_hists_dict[key_name].Write("", root.TObject.kOverwrite)
    rootfile.Close()

#rootfile_path = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment/TPrimeTPrime_M-700/2017_1t1W0Z0H1b.root"
#Rebin(rootfile_path)
