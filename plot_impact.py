import os, sys

mass_points = ["700", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800"]
for mass_point in mass_points:
    os.system("plotImpacts.py -i impacts_"+mass_point+".json -o impacts_"+mass_point)
