import os, sys
import ROOT as root



def custom_crab(config):
    print '>> Customising the crab config'
    config.Site.storageSite = 'T3_US_FNALLPC'
    #config.Site.blacklist = ['SOME_SITE', 'SOME_OTHER_SITE']
    config.JobType.allowUndistributedCMSSW = True
    config.General.workArea = "Crab_Grid_Test"
