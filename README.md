This is the documentation about Combine Analysis for VLQ Search

The workflow of the statistical analysis phase using Combine Tool starts with making datacards-rootfile pairs
for each HypothesisParticle-masspoint. For example, there would be 126 datacrad-rootfile pairs for TPrime-700.

We do it in a sort-of-clumsy-way for now, using the script **BBLite_combine_maker.py**. **BBLite_combine_maker.py**
takes 3 argument inputs - *run_tag*, *signal_mass*, *vlq_particle_typ*e : so, You would run something like - 
***python BBLite_combine_maker.py 2017 1300 T*** to make datacards for TPrime@1300GeV based on 2017 Run. 

After we have made the datacards and partitioned our histograms into 126 channel-wise rootfiles, we need to check - 
1. If there is any bin in any channel with negative norm.
2. If the statistical uncertainty too high in some bins. 

To fix large statistical uncertainty and negative norm, we use the script **histogram_reshapingUtils.py**
To check out how this script is used, have a glance at **prepare_histograms.py**, which calls the functions defined in 
**histogram_reshapingUtils.py**.

After we have done all these, we still have 126 datacard-rootfile pairs per signal mass point. 
And, it is virtually impossible to keep track of them. So, we would want to "merge" 126 datacards
into one single datacard and use one datacard per signal mass point. The script **handle_combine_cards.py** helps us do that.
It takes exactly similar argument inputs as **BBLite_combine_maker.py**. 
In crus, for each signal mass point we would run **BBLite_combine_maker.py**, followed by **handle_combine_cards.py**.
The script **handle_making_datacards.py** does this for you for all the signal mass points. Check that file out and
run only mass-points you wanna run and comment the rest out.