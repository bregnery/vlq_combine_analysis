import os, sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("run_tag", metavar='r', type=int, help="year of run -- 2016 or 2017 or 2018 ?? ")
parser.add_argument("signal_mass", metavar='m', type=int, help="mass of the hypotheses TPrime/BPrime particle in GeV")
parser.add_argument("vlq", metavar='v', help="species of the VLQ -- > T or B")
args = parser.parse_args()
run_tag = args.run_tag
signal_mass = args.signal_mass
vlq = args.vlq
if vlq == "T":
    signal_key = 'TPrimeTPrime_M-'+str(signal_mass)
elif vlq == "B":
    signal_key = 'BPrimeBPrime_M-'+str(signal_mass)
else:
    raise NotImplementedError("Unknown type of VLQ particle Chosen !!! Put in either 'B' or 'T'")


def combine_cards(combine_dir, file_tags, combine_datacard):
    assert os.path.exists(combine_dir)
    assert combine_datacard.endswith(".txt")
    text_files = os.listdir(combine_dir)
    datacards = []
    for text_file_name in text_files:
        text_file_path = os.path.join(combine_dir, text_file_name)
        if text_file_name.endswith(".txt") == False:
            continue
        all_tag_matched = True
        for tag in file_tags:
            if tag not in text_file_name or "Combined" in text_file_name:
                all_tag_matched = False
                break

        if all_tag_matched == False:
            continue
        else:
            datacards.append(text_file_path)
    print(len(datacards))
    combineCards_cmd = "combineCards.py "
    for datacard_path in datacards:
        combineCards_cmd += datacard_path + " "
    combineCards_cmd = combineCards_cmd + "> " + combine_datacard
    #print(combineCards_cmd)
    #print("_________________________________________________________________________")
    os.system(combineCards_cmd)
    #print("Combined the input datacards as "+combine_datacard+" !!!")
    

combine_dir = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/"
if run_tag == 2016:
    combine_dir = os.path.join(combine_dir, "Run2016_Compartment")#/TPrimeTPrime_M-700")
if run_tag == 2017:
    combine_dir = os.path.join(combine_dir, "Run2017_Compartment")
if run_tag == 2018:
    combine_dir = os.path.join(combine_dir, "Run2018_Compartment")
combine_dir = os.path.join(combine_dir,signal_key)
file_tags = []
combine_datacard = "Combined_"+signal_key+"_"+str(run_tag)+".txt"
combine_datacard_path = os.path.join(combine_dir, combine_datacard)
if os.path.exists(combine_datacard_path):
    os.system("rm "+combine_datacard_path)
combine_cards(combine_dir, file_tags, combine_datacard_path)
