import ROOT as root
import os, sys
import argparse

sys.path.append(os.getcwd())
import histogram_reshapingUtils as utils

parser = argparse.ArgumentParser()
parser.add_argument("run_tag", metavar='r', type=int, help="year of run -- 2016 or 2017 or 2018 ?? ")
parser.add_argument("signal_mass", metavar='m', type=int, help="mass of the hypotheses TPrime particle in GeV")
parser.add_argument("vlq", metavar='v', help="species of the VLQ -- > T or B")
args = parser.parse_args()
run_tag = args.run_tag
signal_mass = args.signal_mass
vlq = args.vlq
if vlq == "T":
    signal_key = 'TPrimeTPrime_M-'+str(signal_mass)
elif vlq == "B":
    signal_key = 'BPrimeBPrime_M-'+str(signal_mass)
else:
    raise NotImplementedError("Unknown type of VLQ particle Chosen !!! Put in either 'B' or 'T'")


text_file_path = "Run"+str(run_tag)+"_"+signal_key+".txt"
text_file = open(text_file_path, "r")
rootfiles = text_file.readlines()
rootfiles = [rootfile[:-1] for rootfile in rootfiles]
for rootfile in rootfiles:
    utils.Rebin(rootfile)

