
set -x
set -e
ulimit -s unlimited
ulimit -c 0

function error_exit
{
  if [ $1 -ne 0 ]; then
    echo "Error with exit code ${1}"
    if [ -e FrameworkJobReport.xml ]
    then
      cat << EOF > FrameworkJobReport.xml.tmp
      <FrameworkJobReport>
      <FrameworkError ExitStatus="${1}" Type="" >
      Error with exit code ${1}
      </FrameworkError>
EOF
      tail -n+2 FrameworkJobReport.xml >> FrameworkJobReport.xml.tmp
      mv FrameworkJobReport.xml.tmp FrameworkJobReport.xml
    else
      cat << EOF > FrameworkJobReport.xml
      <FrameworkJobReport>
      <FrameworkError ExitStatus="${1}" Type="" >
      Error with exit code ${1}
      </FrameworkError>
      </FrameworkJobReport>
EOF
    fi
    exit 0
  fi
}

trap 'error_exit $?' ERR

if [ $1 -eq 1 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.05 -s -1 -d workspace_M-700.root -n .Test.POINT.0.05
fi
if [ $1 -eq 2 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.06 -s -1 -d workspace_M-700.root -n .Test.POINT.0.06
fi
if [ $1 -eq 3 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.07 -s -1 -d workspace_M-700.root -n .Test.POINT.0.07
fi
if [ $1 -eq 4 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.08 -s -1 -d workspace_M-700.root -n .Test.POINT.0.08
fi
if [ $1 -eq 5 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.09 -s -1 -d workspace_M-700.root -n .Test.POINT.0.09
fi
if [ $1 -eq 6 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.10 -s -1 -d workspace_M-700.root -n .Test.POINT.0.10
fi
if [ $1 -eq 7 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.11 -s -1 -d workspace_M-700.root -n .Test.POINT.0.11
fi
if [ $1 -eq 8 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.12 -s -1 -d workspace_M-700.root -n .Test.POINT.0.12
fi
if [ $1 -eq 9 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.13 -s -1 -d workspace_M-700.root -n .Test.POINT.0.13
fi
if [ $1 -eq 10 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.14 -s -1 -d workspace_M-700.root -n .Test.POINT.0.14
fi
if [ $1 -eq 11 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.15 -s -1 -d workspace_M-700.root -n .Test.POINT.0.15
fi
if [ $1 -eq 12 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.16 -s -1 -d workspace_M-700.root -n .Test.POINT.0.16
fi
if [ $1 -eq 13 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.17 -s -1 -d workspace_M-700.root -n .Test.POINT.0.17
fi
if [ $1 -eq 14 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.18 -s -1 -d workspace_M-700.root -n .Test.POINT.0.18
fi
if [ $1 -eq 15 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.19 -s -1 -d workspace_M-700.root -n .Test.POINT.0.19
fi
if [ $1 -eq 16 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.20 -s -1 -d workspace_M-700.root -n .Test.POINT.0.20
fi
if [ $1 -eq 17 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.21 -s -1 -d workspace_M-700.root -n .Test.POINT.0.21
fi
if [ $1 -eq 18 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.22 -s -1 -d workspace_M-700.root -n .Test.POINT.0.22
fi
if [ $1 -eq 19 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.23 -s -1 -d workspace_M-700.root -n .Test.POINT.0.23
fi
if [ $1 -eq 20 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.24 -s -1 -d workspace_M-700.root -n .Test.POINT.0.24
fi
if [ $1 -eq 21 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.25 -s -1 -d workspace_M-700.root -n .Test.POINT.0.25
fi
if [ $1 -eq 22 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.26 -s -1 -d workspace_M-700.root -n .Test.POINT.0.26
fi
if [ $1 -eq 23 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.27 -s -1 -d workspace_M-700.root -n .Test.POINT.0.27
fi
if [ $1 -eq 24 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.28 -s -1 -d workspace_M-700.root -n .Test.POINT.0.28
fi
if [ $1 -eq 25 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.29 -s -1 -d workspace_M-700.root -n .Test.POINT.0.29
fi
if [ $1 -eq 26 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.30 -s -1 -d workspace_M-700.root -n .Test.POINT.0.30
fi
if [ $1 -eq 27 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.31 -s -1 -d workspace_M-700.root -n .Test.POINT.0.31
fi
if [ $1 -eq 28 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.32 -s -1 -d workspace_M-700.root -n .Test.POINT.0.32
fi
if [ $1 -eq 29 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.33 -s -1 -d workspace_M-700.root -n .Test.POINT.0.33
fi
if [ $1 -eq 30 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.34 -s -1 -d workspace_M-700.root -n .Test.POINT.0.34
fi
if [ $1 -eq 31 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.35 -s -1 -d workspace_M-700.root -n .Test.POINT.0.35
fi
if [ $1 -eq 32 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.36 -s -1 -d workspace_M-700.root -n .Test.POINT.0.36
fi
if [ $1 -eq 33 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.37 -s -1 -d workspace_M-700.root -n .Test.POINT.0.37
fi
if [ $1 -eq 34 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.38 -s -1 -d workspace_M-700.root -n .Test.POINT.0.38
fi
if [ $1 -eq 35 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.39 -s -1 -d workspace_M-700.root -n .Test.POINT.0.39
fi
if [ $1 -eq 36 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.40 -s -1 -d workspace_M-700.root -n .Test.POINT.0.40
fi
if [ $1 -eq 37 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.41 -s -1 -d workspace_M-700.root -n .Test.POINT.0.41
fi
if [ $1 -eq 38 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.42 -s -1 -d workspace_M-700.root -n .Test.POINT.0.42
fi
if [ $1 -eq 39 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.43 -s -1 -d workspace_M-700.root -n .Test.POINT.0.43
fi
if [ $1 -eq 40 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.44 -s -1 -d workspace_M-700.root -n .Test.POINT.0.44
fi
if [ $1 -eq 41 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.45 -s -1 -d workspace_M-700.root -n .Test.POINT.0.45
fi
if [ $1 -eq 42 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.46 -s -1 -d workspace_M-700.root -n .Test.POINT.0.46
fi
if [ $1 -eq 43 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.47 -s -1 -d workspace_M-700.root -n .Test.POINT.0.47
fi
if [ $1 -eq 44 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.48 -s -1 -d workspace_M-700.root -n .Test.POINT.0.48
fi
if [ $1 -eq 45 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.49 -s -1 -d workspace_M-700.root -n .Test.POINT.0.49
fi
if [ $1 -eq 46 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.50 -s -1 -d workspace_M-700.root -n .Test.POINT.0.50
fi
if [ $1 -eq 47 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.51 -s -1 -d workspace_M-700.root -n .Test.POINT.0.51
fi
if [ $1 -eq 48 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.52 -s -1 -d workspace_M-700.root -n .Test.POINT.0.52
fi
if [ $1 -eq 49 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.53 -s -1 -d workspace_M-700.root -n .Test.POINT.0.53
fi
if [ $1 -eq 50 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.54 -s -1 -d workspace_M-700.root -n .Test.POINT.0.54
fi
if [ $1 -eq 51 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.55 -s -1 -d workspace_M-700.root -n .Test.POINT.0.55
fi
if [ $1 -eq 52 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.56 -s -1 -d workspace_M-700.root -n .Test.POINT.0.56
fi
if [ $1 -eq 53 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.57 -s -1 -d workspace_M-700.root -n .Test.POINT.0.57
fi
if [ $1 -eq 54 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.58 -s -1 -d workspace_M-700.root -n .Test.POINT.0.58
fi
if [ $1 -eq 55 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.59 -s -1 -d workspace_M-700.root -n .Test.POINT.0.59
fi
if [ $1 -eq 56 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.60 -s -1 -d workspace_M-700.root -n .Test.POINT.0.60
fi
if [ $1 -eq 57 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.61 -s -1 -d workspace_M-700.root -n .Test.POINT.0.61
fi
if [ $1 -eq 58 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.62 -s -1 -d workspace_M-700.root -n .Test.POINT.0.62
fi
if [ $1 -eq 59 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.63 -s -1 -d workspace_M-700.root -n .Test.POINT.0.63
fi
if [ $1 -eq 60 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.64 -s -1 -d workspace_M-700.root -n .Test.POINT.0.64
fi
if [ $1 -eq 61 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.65 -s -1 -d workspace_M-700.root -n .Test.POINT.0.65
fi
if [ $1 -eq 62 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.66 -s -1 -d workspace_M-700.root -n .Test.POINT.0.66
fi
if [ $1 -eq 63 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.67 -s -1 -d workspace_M-700.root -n .Test.POINT.0.67
fi
if [ $1 -eq 64 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.68 -s -1 -d workspace_M-700.root -n .Test.POINT.0.68
fi
if [ $1 -eq 65 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.69 -s -1 -d workspace_M-700.root -n .Test.POINT.0.69
fi
if [ $1 -eq 66 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.70 -s -1 -d workspace_M-700.root -n .Test.POINT.0.70
fi
if [ $1 -eq 67 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.71 -s -1 -d workspace_M-700.root -n .Test.POINT.0.71
fi
if [ $1 -eq 68 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.72 -s -1 -d workspace_M-700.root -n .Test.POINT.0.72
fi
if [ $1 -eq 69 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.73 -s -1 -d workspace_M-700.root -n .Test.POINT.0.73
fi
if [ $1 -eq 70 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.74 -s -1 -d workspace_M-700.root -n .Test.POINT.0.74
fi
if [ $1 -eq 71 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.75 -s -1 -d workspace_M-700.root -n .Test.POINT.0.75
fi
if [ $1 -eq 72 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.76 -s -1 -d workspace_M-700.root -n .Test.POINT.0.76
fi
if [ $1 -eq 73 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.77 -s -1 -d workspace_M-700.root -n .Test.POINT.0.77
fi
if [ $1 -eq 74 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.78 -s -1 -d workspace_M-700.root -n .Test.POINT.0.78
fi
if [ $1 -eq 75 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.79 -s -1 -d workspace_M-700.root -n .Test.POINT.0.79
fi
if [ $1 -eq 76 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.80 -s -1 -d workspace_M-700.root -n .Test.POINT.0.80
fi
if [ $1 -eq 77 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.81 -s -1 -d workspace_M-700.root -n .Test.POINT.0.81
fi
if [ $1 -eq 78 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.82 -s -1 -d workspace_M-700.root -n .Test.POINT.0.82
fi
if [ $1 -eq 79 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.83 -s -1 -d workspace_M-700.root -n .Test.POINT.0.83
fi
if [ $1 -eq 80 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.84 -s -1 -d workspace_M-700.root -n .Test.POINT.0.84
fi
if [ $1 -eq 81 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.85 -s -1 -d workspace_M-700.root -n .Test.POINT.0.85
fi
if [ $1 -eq 82 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.86 -s -1 -d workspace_M-700.root -n .Test.POINT.0.86
fi
if [ $1 -eq 83 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.87 -s -1 -d workspace_M-700.root -n .Test.POINT.0.87
fi
if [ $1 -eq 84 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.88 -s -1 -d workspace_M-700.root -n .Test.POINT.0.88
fi
if [ $1 -eq 85 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.89 -s -1 -d workspace_M-700.root -n .Test.POINT.0.89
fi
if [ $1 -eq 86 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.90 -s -1 -d workspace_M-700.root -n .Test.POINT.0.90
fi
if [ $1 -eq 87 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.91 -s -1 -d workspace_M-700.root -n .Test.POINT.0.91
fi
if [ $1 -eq 88 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.92 -s -1 -d workspace_M-700.root -n .Test.POINT.0.92
fi
if [ $1 -eq 89 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.93 -s -1 -d workspace_M-700.root -n .Test.POINT.0.93
fi
if [ $1 -eq 90 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.94 -s -1 -d workspace_M-700.root -n .Test.POINT.0.94
fi
if [ $1 -eq 91 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.95 -s -1 -d workspace_M-700.root -n .Test.POINT.0.95
fi
if [ $1 -eq 92 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.96 -s -1 -d workspace_M-700.root -n .Test.POINT.0.96
fi
if [ $1 -eq 93 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.97 -s -1 -d workspace_M-700.root -n .Test.POINT.0.97
fi
if [ $1 -eq 94 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.98 -s -1 -d workspace_M-700.root -n .Test.POINT.0.98
fi
if [ $1 -eq 95 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 0.99 -s -1 -d workspace_M-700.root -n .Test.POINT.0.99
fi
if [ $1 -eq 96 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.00 -s -1 -d workspace_M-700.root -n .Test.POINT.1.00
fi
if [ $1 -eq 97 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.01 -s -1 -d workspace_M-700.root -n .Test.POINT.1.01
fi
if [ $1 -eq 98 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.02 -s -1 -d workspace_M-700.root -n .Test.POINT.1.02
fi
if [ $1 -eq 99 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.03 -s -1 -d workspace_M-700.root -n .Test.POINT.1.03
fi
if [ $1 -eq 100 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.04 -s -1 -d workspace_M-700.root -n .Test.POINT.1.04
fi
if [ $1 -eq 101 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.05 -s -1 -d workspace_M-700.root -n .Test.POINT.1.05
fi
if [ $1 -eq 102 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.06 -s -1 -d workspace_M-700.root -n .Test.POINT.1.06
fi
if [ $1 -eq 103 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.07 -s -1 -d workspace_M-700.root -n .Test.POINT.1.07
fi
if [ $1 -eq 104 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.08 -s -1 -d workspace_M-700.root -n .Test.POINT.1.08
fi
if [ $1 -eq 105 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.09 -s -1 -d workspace_M-700.root -n .Test.POINT.1.09
fi
if [ $1 -eq 106 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.10 -s -1 -d workspace_M-700.root -n .Test.POINT.1.10
fi
if [ $1 -eq 107 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.11 -s -1 -d workspace_M-700.root -n .Test.POINT.1.11
fi
if [ $1 -eq 108 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.12 -s -1 -d workspace_M-700.root -n .Test.POINT.1.12
fi
if [ $1 -eq 109 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.13 -s -1 -d workspace_M-700.root -n .Test.POINT.1.13
fi
if [ $1 -eq 110 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.14 -s -1 -d workspace_M-700.root -n .Test.POINT.1.14
fi
if [ $1 -eq 111 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.15 -s -1 -d workspace_M-700.root -n .Test.POINT.1.15
fi
if [ $1 -eq 112 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.16 -s -1 -d workspace_M-700.root -n .Test.POINT.1.16
fi
if [ $1 -eq 113 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.17 -s -1 -d workspace_M-700.root -n .Test.POINT.1.17
fi
if [ $1 -eq 114 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.18 -s -1 -d workspace_M-700.root -n .Test.POINT.1.18
fi
if [ $1 -eq 115 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.19 -s -1 -d workspace_M-700.root -n .Test.POINT.1.19
fi
if [ $1 -eq 116 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.20 -s -1 -d workspace_M-700.root -n .Test.POINT.1.20
fi
if [ $1 -eq 117 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.21 -s -1 -d workspace_M-700.root -n .Test.POINT.1.21
fi
if [ $1 -eq 118 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.22 -s -1 -d workspace_M-700.root -n .Test.POINT.1.22
fi
if [ $1 -eq 119 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.23 -s -1 -d workspace_M-700.root -n .Test.POINT.1.23
fi
if [ $1 -eq 120 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.24 -s -1 -d workspace_M-700.root -n .Test.POINT.1.24
fi
if [ $1 -eq 121 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.25 -s -1 -d workspace_M-700.root -n .Test.POINT.1.25
fi
if [ $1 -eq 122 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.26 -s -1 -d workspace_M-700.root -n .Test.POINT.1.26
fi
if [ $1 -eq 123 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.27 -s -1 -d workspace_M-700.root -n .Test.POINT.1.27
fi
if [ $1 -eq 124 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.28 -s -1 -d workspace_M-700.root -n .Test.POINT.1.28
fi
if [ $1 -eq 125 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.29 -s -1 -d workspace_M-700.root -n .Test.POINT.1.29
fi
if [ $1 -eq 126 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.30 -s -1 -d workspace_M-700.root -n .Test.POINT.1.30
fi
if [ $1 -eq 127 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.31 -s -1 -d workspace_M-700.root -n .Test.POINT.1.31
fi
if [ $1 -eq 128 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.32 -s -1 -d workspace_M-700.root -n .Test.POINT.1.32
fi
if [ $1 -eq 129 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.33 -s -1 -d workspace_M-700.root -n .Test.POINT.1.33
fi
if [ $1 -eq 130 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.34 -s -1 -d workspace_M-700.root -n .Test.POINT.1.34
fi
if [ $1 -eq 131 ]; then
  ./combine --LHCmode LHC-limits -T 500 --saveToys --saveHybridResult --bypassFrequentistFit -M HybridNew -m 700 --singlePoint 1.35 -s -1 -d workspace_M-700.root -n .Test.POINT.1.35
fi
tar -cf combine_output.tar higgsCombine*.root
rm higgsCombine*.root
