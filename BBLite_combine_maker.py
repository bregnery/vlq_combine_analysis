import math
import gc, os, sys
import ROOT 
from array import array
import numpy as np
import argparse
from ROOT import TColor
from ROOT import TGaxis
from ROOT import THStack
from collections import defaultdict

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1;")
ROOT.TH1.AddDirectory(ROOT.kFALSE)
ROOT.gStyle.SetOptStat(0)
TGaxis.SetMaxDigits(2)

this_dir = os.getcwd()
sys.path.append(this_dir)
sys.path.append("/uscms/home/abhd1994/MyAnalysis/CMSSW_10_2_18/src/VLQAnalysis/FinalStateAnalysis/Analysis_Utilities")
import compute_histograms_onMC as mh

parser = argparse.ArgumentParser()
parser.add_argument("run_tag", metavar='r', type=int, help="year of run -- 2016 or 2017 or 2018 ?? ")
parser.add_argument("signal_mass", metavar='m', type=int, help="mass of the hypotheses TPrime particle in GeV")
parser.add_argument("vlq", metavar='v', help="species of the VLQ -- > T or B")
args = parser.parse_args()
run_tag = args.run_tag
signal_mass = args.signal_mass
vlq = args.vlq
if vlq == "T":
    signal_key = 'TPrimeTPrime_M-'+str(signal_mass)
elif vlq == "B":
    signal_key = 'BPrimeBPrime_M-'+str(signal_mass)
else:
    raise NotImplementedError("Unknown type of VLQ particle Chosen !!! Put in either 'B' or 'T'")

#year=['2016','2017','2018','All']
#LumiErr = [0.025, 0.023, 0.025, 0.018]
LHCRun_base_dirs = {}
LHCRun_base_dirs["2016"] = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/Run2016_Compartment/"
LHCRun_base_dirs["2017"] = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/Run2017_Compartment/"
LHCRun_base_dirs["2018"] = "/uscms_data/d1/abhd1994/YOURWORKINGAREA/Run2018_Compartment/"

year = ["2017"]
LumiErr = [0.023]
regions = mh.all_region_tags
channels = ["4_AK8"]
variables = ["HT"]
#Q2scale, pdf
systematic_error_sources = ["lumi", "pileupReweight", "jec", "jer", "DDSystematic",
                            "HiggsTagSF", "HiggsMistagSF", "topTagSF", "topMistagSF",
                            "WTagSF", "WMistagSF", "ZTagSF", "ZMistagSF",
                            "BTagSF", "BMistagSF", "QCDTagSF", "QCDMistagSF"]

Samples_Dict = defaultdict(list)
generic_Samples = ['DD-Multijet.root', 'WJets.root','ZJets.root', 'ttWJets.root', 
           'ttWW.root', 'ttWZ.root', 'ttZJets.root', 'ttZZ.root', 'ttbar.root','data_obs.root']


Samples_Dict['TPrimeTPrime_M-700'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1000'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1100'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1200'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1300'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1400'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1500'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1600'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1700'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-1800'] = generic_Samples[::]
Samples_Dict['TPrimeTPrime_M-700'].insert(0, 'TPrimeTPrime_M-700.root')
Samples_Dict['TPrimeTPrime_M-1000'].insert(0, 'TPrimeTPrime_M-1000.root')
Samples_Dict['TPrimeTPrime_M-1100'].insert(0, 'TPrimeTPrime_M-1100.root')
Samples_Dict['TPrimeTPrime_M-1200'].insert(0, 'TPrimeTPrime_M-1200.root')
Samples_Dict['TPrimeTPrime_M-1300'].insert(0, 'TPrimeTPrime_M-1300.root')
Samples_Dict['TPrimeTPrime_M-1400'].insert(0, 'TPrimeTPrime_M-1400.root')
Samples_Dict['TPrimeTPrime_M-1500'].insert(0, 'TPrimeTPrime_M-1500.root')
Samples_Dict['TPrimeTPrime_M-1600'].insert(0, 'TPrimeTPrime_M-1600.root')
Samples_Dict['TPrimeTPrime_M-1700'].insert(0, 'TPrimeTPrime_M-1700.root')
Samples_Dict['TPrimeTPrime_M-1800'].insert(0, 'TPrimeTPrime_M-1800.root')

Samples_Dict['BPrimeBPrime_M-900'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1000'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1100'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1200'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1300'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1400'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1500'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1600'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-1700'] = generic_Samples[::]
Samples_Dict['BPrimeBPrime_M-900'].insert(0, 'BPrimeBPrime_M-900.root')
Samples_Dict['BPrimeBPrime_M-1000'].insert(0, 'BPrimeBPrime_M-1000.root')
Samples_Dict['BPrimeBPrime_M-1100'].insert(0, 'BPrimeBPrime_M-1100.root')
Samples_Dict['BPrimeBPrime_M-1200'].insert(0, 'BPrimeBPrime_M-1200.root')
Samples_Dict['BPrimeBPrime_M-1300'].insert(0, 'BPrimeBPrime_M-1300.root')
Samples_Dict['BPrimeBPrime_M-1400'].insert(0, 'BPrimeBPrime_M-1400.root')
Samples_Dict['BPrimeBPrime_M-1500'].insert(0, 'BPrimeBPrime_M-1500.root')
Samples_Dict['BPrimeBPrime_M-1600'].insert(0, 'BPrimeBPrime_M-1600.root')
Samples_Dict['BPrimeBPrime_M-1700'].insert(0, 'BPrimeBPrime_M-1700.root')




SamplesNameCombined_Dict = defaultdict(list)
generic_analysis_samples = [ 'DataDriven-Multijet', 'WJets', 'ZJets', 'ttWJets', 'ttWW' , 'ttWZ', 'ttZJets', 'ttZZ', 'ttbar', 'data_obs']
SamplesNameCombined_Dict['TPrimeTPrime_M-700'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1000'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1100'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1200'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1300'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1400'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1500'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1600'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1700'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-1800'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['TPrimeTPrime_M-700'].insert(0, 'TPrimeTPrime_M-700')
SamplesNameCombined_Dict['TPrimeTPrime_M-1000'].insert(0, 'TPrimeTPrime_M-1000')
SamplesNameCombined_Dict['TPrimeTPrime_M-1100'].insert(0, 'TPrimeTPrime_M-1100')
SamplesNameCombined_Dict['TPrimeTPrime_M-1200'].insert(0, 'TPrimeTPrime_M-1200')
SamplesNameCombined_Dict['TPrimeTPrime_M-1300'].insert(0, 'TPrimeTPrime_M-1300')
SamplesNameCombined_Dict['TPrimeTPrime_M-1400'].insert(0, 'TPrimeTPrime_M-1400')
SamplesNameCombined_Dict['TPrimeTPrime_M-1500'].insert(0, 'TPrimeTPrime_M-1500')
SamplesNameCombined_Dict['TPrimeTPrime_M-1600'].insert(0, 'TPrimeTPrime_M-1600')
SamplesNameCombined_Dict['TPrimeTPrime_M-1700'].insert(0, 'TPrimeTPrime_M-1700')
SamplesNameCombined_Dict['TPrimeTPrime_M-1800'].insert(0, 'TPrimeTPrime_M-1800')

SamplesNameCombined_Dict['BPrimeBPrime_M-900'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1000'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1100'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1200'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1300'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1400'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1500'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1600'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-1700'] = generic_analysis_samples[::]
SamplesNameCombined_Dict['BPrimeBPrime_M-900'].insert(0, 'BPrimeBPrime_M-900')
SamplesNameCombined_Dict['BPrimeBPrime_M-1000'].insert(0, 'BPrimeBPrime_M-1000')
SamplesNameCombined_Dict['BPrimeBPrime_M-1100'].insert(0, 'BPrimeBPrime_M-1100')
SamplesNameCombined_Dict['BPrimeBPrime_M-1200'].insert(0, 'BPrimeBPrime_M-1200')
SamplesNameCombined_Dict['BPrimeBPrime_M-1300'].insert(0, 'BPrimeBPrime_M-1300')
SamplesNameCombined_Dict['BPrimeBPrime_M-1400'].insert(0, 'BPrimeBPrime_M-1400')
SamplesNameCombined_Dict['BPrimeBPrime_M-1500'].insert(0, 'BPrimeBPrime_M-1500')
SamplesNameCombined_Dict['BPrimeBPrime_M-1600'].insert(0, 'BPrimeBPrime_M-1600')
SamplesNameCombined_Dict['BPrimeBPrime_M-1700'].insert(0, 'BPrimeBPrime_M-1700')


if not os.path.exists('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2016_Compartment'):
    os.mkdir('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2016_Compartment')
if not os.path.exists('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment'):
    os.mkdir('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment')
if not os.path.exists('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2018_Compartment'):
    os.mkdir('/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2018_Compartment')

if run_tag == 2016:
    storage_base_dir = '/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2016_Compartment'
elif run_tag == 2017:
    storage_base_dir = '/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2017_Compartment'
elif run_tag == 2018:
    storage_base_dir = '/uscms_data/d1/abhd1994/YOURWORKINGAREA/CombinedFiles/Run2018_Compartment'

storage_dir = os.path.join(storage_base_dir, signal_key)
if not os.path.exists(storage_dir):
    os.mkdir(storage_dir)

Samples = Samples_Dict[signal_key]
SamplesNameCombined = SamplesNameCombined_Dict[signal_key]


Syst_Source_Map = defaultdict(dict)
for namesys in systematic_error_sources:
    Syst_Source_Map[namesys] = {}
    for sample in SamplesNameCombined:
        if sample == 'data_obs':
            Syst_Source_Map[namesys][sample] = "-"
        elif sample == 'DataDriven-Multijet':
            if namesys == 'jec' or namesys == 'DDSystematic':
                Syst_Source_Map[namesys][sample] = "1"
            else:
                Syst_Source_Map[namesys][sample] = "-"
        else:
            if namesys == 'DDSystematic':
                Syst_Source_Map[namesys][sample] = "-"
            elif namesys == 'lumi':
                Syst_Source_Map[namesys][sample] = "1.025"
            else:
                Syst_Source_Map[namesys][sample]= "1"


for i in range(len(Samples)):
    Samples[i] = 'HT_histograms_'+Samples[i]

#SamplesName = ['TPrimeTPrime_700','DD-Multijet','WJets', 'ZJets', 'ttWJets', 'ttWW' , 'ttWZ', 'ttZJets', 'ttZZ', 'ttbar', 'data']

colors =  [ROOT.kBlack,ROOT.kYellow,ROOT.kGreen,ROOT.kBlue-3,ROOT.kRed-4,ROOT.kOrange-3, ROOT.kOrange-6, 
           ROOT.kCyan-6, ROOT.kOrange-6, ROOT.kCyan-6, ROOT.kOrange-6, ROOT.kCyan-6, ROOT.kPink-1, ROOT.kBlue+3,
           ROOT.kRed+1, ROOT.kGreen+3, ROOT.kBlue+1, ROOT.kTeal+2, ROOT.kMagenta+3]

Hists = []
HistsSysUp = []
HistsSysDown = []
Hists_copy =[]

#data_obs_histogram = None
#data_obs_histogram_integral = -1.

for numyear, nameyear in enumerate(year):
    l0 = []
    copyl0 = []
    SysUpl0 = []
    SysDownl0 = []
    Files = []
    for f in range(len(Samples)):
        l1 = []
        copyl1 = []
        SysUpl1 = []
        SysDownl1 = []
        rootfile_path = os.path.join(LHCRun_base_dirs[nameyear], Samples[f])
        print("*** ",rootfile_path)
        Files.append(ROOT.TFile.Open(rootfile_path, "READ"))
        for numch, namech in enumerate(channels):
            l2=[]
            copyl2=[]
            SysUpl2=[]
            SysDownl2=[]
            for numreg, namereg in enumerate(regions):
                l3=[]
                copyl3=[]
                SysUpl3=[]
                SysDownl3=[]
                for numvar, namevar in enumerate(variables):
                    SysUpl4=[]
                    SysDownl4=[]
                    #print(len(list(Files[f].GetListOfKeys())))
                    h = Files[f].Get(namereg + '_' + namevar)
                    #h.SetBinContent(h.GetXaxis().GetNbins(), h.GetBinContent(h.GetXaxis().GetNbins()) + h.GetBinContent(h.GetXaxis().GetNbins()+1))
                    l3.append(h)
                    copyl3.append(h.Clone())
                    for numsys, namesys in enumerate(systematic_error_sources):
                        if namesys == "lumi":
                            continue
                        h = Files[f].Get(namereg + '_' + namevar+ '_' + namesys+ '_Up')
                        #print(h.GetEntries())
                        #h.SetBinContent(h.GetXaxis().GetNbins(), h.GetBinContent(h.GetXaxis().GetNbins()) + h.GetBinContent(h.GetXaxis().GetNbins()+1))
                        SysUpl4.append(h)
                        h = Files[f].Get(namereg + '_' + namevar+ '_' + namesys+ '_Down')
                        #h.SetBinContent(h.GetXaxis().GetNbins(), h.GetBinContent(h.GetXaxis().GetNbins()) + h.GetBinContent(h.GetXaxis().GetNbins()+1))
                        SysDownl4.append(h)
                    SysUpl3.append(SysUpl4)
                    SysDownl3.append(SysDownl4)
                l2.append(l3)
                copyl2.append(copyl3)
                SysUpl2.append(SysUpl3)
                SysDownl2.append(SysDownl3)
            l1.append(l2)
            copyl1.append(copyl2)
            SysUpl1.append(SysUpl2)
            SysDownl1.append(SysDownl2)
        l0.append(l1)
        copyl0.append(copyl1)
        SysUpl0.append(SysUpl1)
        SysDownl0.append(SysDownl1)
    Hists.append(l0)
    Hists_copy.append(copyl0)
    HistsSysUp.append(SysUpl0)       
    HistsSysDown.append(SysDownl0)
    

statName={}

for numyear, nameyear in enumerate(year):
    for numreg, namereg in enumerate(regions):
        file_name = os.path.join(storage_dir, nameyear+'_'+namereg+'.root')
        hfile = ROOT.TFile( file_name, 'RECREATE', 'combine input histograms' )
        blinding_bkg_sum = Hists[numyear][1][0][numreg][0].Clone()
        
        for i in range(blinding_bkg_sum.GetNbinsX()+1):
            blinding_bkg_sum.SetBinContent(i, 0.0)
            blinding_bkg_sum.SetBinError(i, 0.0)
        for f in range(len(Samples)):
            Hists[numyear][f][0][numreg][0].SetName(SamplesNameCombined[f])
            if f >= 1 and f < (len(Samples) - 1) :
                #print(SamplesNameCombined[f])
                blinding_bkg_sum.Add(blinding_bkg_sum, Hists[numyear][f][0][numreg][0], 1, 1)
            if f < (len(Samples)) : # if f < (len(Samples) - 1) for replacing data_obs with sum_of_bkgs
                Hists[numyear][f][0][numreg][0].Write()
            """
            else:
                blinding_bkg_sum.SetName(SamplesNameCombined[f])
                blinding_bkg_sum.Write()
            """
            #if f==0:
            #continue
            for numsys, namesys in enumerate(systematic_error_sources):
                if namesys == 'lumi':
                    continue
                numsys -= 1
                HistsSysUp[numyear][f][0][numreg][0][numsys].SetName(SamplesNameCombined[f] + '_' + namesys + 'Up')
                HistsSysDown[numyear][f][0][numreg][0][numsys].SetName(SamplesNameCombined[f] + '_' + namesys + 'Down')
                HistsSysUp[numyear][f][0][numreg][0][numsys].Write()
                HistsSysDown[numyear][f][0][numreg][0][numsys].Write()
        

        for f in range(len(Samples)):
            #if f == 0:
            #continue
            #print(Samples[f], SamplesNameCombined[f])
            stat=[]
            for b in range(Hists[numyear][f][0][numreg][0].GetNbinsX()):
                if Hists[numyear][f][0][numreg][0].GetBinContent(b+1)==0:
                    continue
                    #Hists[numyear][f][0][numreg][0].SetBinContent(b+1, 1.e-6)
                HstatUp = Hists[numyear][f][0][numreg][0].Clone()
                #print(SamplesNameCombined[f+2])
                HstatUp.SetBinContent(b+1,HstatUp.GetBinContent(b+1) + HstatUp.GetBinError(b+1))
                HstatUp.SetName(SamplesNameCombined[f]+ '_' + SamplesNameCombined[f]+'StatBin' +str(b+1)+ 'Up')
                HstatUp.Write()
                HstatDown = Hists[numyear][f][0][numreg][0].Clone()
                HstatDown.SetBinContent(b+1,HstatDown.GetBinContent(b+1) + HstatDown.GetBinError(b+1))
                HstatDown.SetName(SamplesNameCombined[f]+ '_' + SamplesNameCombined[f]+'StatBin' +str(b+1)+ 'Down')
                HstatDown.Write()
                stat.append(SamplesNameCombined[f]+'StatBin' +str(b+1))
            statName[nameyear + namereg + SamplesNameCombined[f]]=stat
        hfile.Write()
        hfile.Close()




#print(statName)
#sys.exit()
#SignalSamples = ['700', '800', '900', '1000', '1100', '1200', '1300', '1400', '1500', '1600', '1700']

#SignalSamples = ['700']#, '1200']#, '1100', '1200', '1500']
SignalSamples = [str(signal_mass)]
for i in range(len(SignalSamples)):
    if vlq == 'T':
        SignalSamples[i] = 'HT_histograms_TPrimeTPrime_M-'+SignalSamples[i]
    elif vlq == 'B':
        SignalSamples[i] = 'HT_histograms_BPrimeBPrime_M-'+SignalSamples[i]

"""                                                                                                                                                                                                       
Samples = ['data.root', 'DataDriven-Multijet.root', 'WJets.root','ZJets.root', 'ttWJets.root', 'ttWW.root', 'ttWZ.root', 'ttZJets.root', 'ttZZ.root', 'ttbar.root',  'TPrimeTPrime_M-1000.root', 'TPrimeTPrime_M-1100.root', 'TPrimeTPrime_M-1200.root', 'TPrimeTPrime_M-1300.root', 'TPrimeTPrime_M-1400.root', 'TPrimeTPrime_M-1500.root', 'TPrimeTPrime_M-1600.root', 'TPrimeTPrime_M-1700.root']                                                                                                                                        
"""
expected_bkg = defaultdict(dict)
for numyear, nameyear in enumerate(year):
    expected_bkg[nameyear] = {}
    for numreg, namereg in enumerate(regions):
        expected_bkg[nameyear][namereg] = Hists[numyear][1][0][numreg][0].Integral() +\
                                          Hists[numyear][2][0][numreg][0].Integral() +\
                                          Hists[numyear][3][0][numreg][0].Integral() +\
                                          Hists[numyear][4][0][numreg][0].Integral() +\
                                          Hists[numyear][5][0][numreg][0].Integral() +\
                                          Hists[numyear][6][0][numreg][0].Integral() +\
                                          Hists[numyear][7][0][numreg][0].Integral() +\
                                          Hists[numyear][8][0][numreg][0].Integral() +\
                                          Hists[numyear][9][0][numreg][0].Integral() 


sum_of_bkgs   = defaultdict(dict)
nonzero_bkgs  = defaultdict(dict)
observed_data = defaultdict(dict)
if vlq == 'T':
    sig_label = 'TPrimeTPrime'
elif vlq == 'B':
    sig_label = 'BPrimeBPrime'

for numyear, nameyear in enumerate(year):
    nonzero_bkgs[nameyear] = defaultdict(dict)
    sum_of_bkgs[nameyear]  = {}
    observed_data[nameyear]= {} 
    for numreg, namereg in enumerate(regions):
        nonzero_bkgs[nameyear][namereg] = defaultdict(float)
        sum_of_bkgs[nameyear][namereg]  = 0
        observed_data[nameyear][namereg]= 0
        for i, key in enumerate(SamplesNameCombined):
            if key == 'data_obs':
                observed_data[nameyear][namereg] = Hists[numyear][i][0][numreg][0].Integral()
                continue
            total_entries = Hists[numyear][i][0][numreg][0].GetEntries()
            hist_integral = Hists[numyear][i][0][numreg][0].Integral()
            if (total_entries < 1 or hist_integral<= 0) and sig_label not in key:
                #print("## ", total_entries, key)
                continue
            else:
                #print(namereg, key, Hists[numyear][i][0][numreg][0].Integral())
                #print(nonzero_bkgs[nameyear][namereg])
                nonzero_bkgs[nameyear][namereg][key] = Hists[numyear][i][0][numreg][0].Integral()
                if sig_label not in key:
                    sum_of_bkgs[nameyear][namereg] += nonzero_bkgs[nameyear][namereg][key]



#print(sum_of_bkgs)
NULL_OBS_REGS = []
data_obs_histogram = None
data_obs_histogram_integral = -1.

for namesig_ in SignalSamples:
    for numyear, nameyear in enumerate(year):
        for numreg, namereg in enumerate(regions):
            data_obs_histogram_integral = Samples
            cardName = namesig_[14:]+'_'+nameyear+'_' + namereg
            #print(Samples, namesig_ + '.root')
            #print("@@@@")
            Sid= Samples.index(namesig_ + '.root')
            namesig = namesig_[14:]
            num_nonzero_hists = len(nonzero_bkgs[nameyear][namereg].keys())
            bin_headline = [cardName[:-16].ljust(21) for i in range(num_nonzero_hists)]
            bin_headline = 'bin'.ljust(35) + ''.join(bin_headline)
            process_idx_headline = [str(i).ljust(21) for i in range(num_nonzero_hists)]
            process_idx_headline = 'process'.ljust(35) + ''.join(process_idx_headline)
            process_keys = list(nonzero_bkgs[nameyear][namereg].keys())
            #print(process_keys , namesig)
            signal_Pid = process_keys.index(namesig)
            process_keys.pop(signal_Pid)
            process_headline = [key.ljust(21) for key in process_keys]
            process_headline = 'process'.ljust(35) + namesig.ljust(21) + ''.join(process_headline)
            rates = []
            rates.append(str(nonzero_bkgs[nameyear][namereg][namesig]).ljust(18))
            for key in process_keys:
                full_precision_rate = nonzero_bkgs[nameyear][namereg][key]
                limited_precision_rate = "{:.6f}".format(full_precision_rate)
                rates.append(str(limited_precision_rate).ljust(18))
            rates_headline = 'rate'.ljust(35) + ''.join(rates)
            systematic_lines = []
            for namesys in systematic_error_sources:
                syst_rep = ''
                if namesys == 'lumi':
                    syst_rep += namesys.ljust(28) + 'lnN'.ljust(15) + Syst_Source_Map[namesys][namesig].ljust(20)
                else:
                    syst_rep += namesys.ljust(28) + 'shape'.ljust(15) + Syst_Source_Map[namesys][namesig].ljust(20)
                    
                for proc in process_keys:
                    syst_rep += Syst_Source_Map[namesys][proc].ljust(20)
                systematic_lines.append(syst_rep)
            if sum_of_bkgs[nameyear][namereg] == 0:
                NULL_OBS_REGS.append(namereg)
                continue
            T1 = 'max 1 number of categories \n' +\
                 'jmax * number of samples minus one\n' +\
                 'kmax * number of nuisance parameters\n' +\
                 '------------\n'+\
                 'shapes * * '  + nameyear+'_'+namereg+'.root' + ' $PROCESS $PROCESS_$SYSTEMATIC\n' +\
                 '------------\n'+\
                 'bin'.ljust(35) + cardName[:-16] + '\n'+\
                 'observation'.ljust(35) + str(observed_data[nameyear][namereg]) + '\n'+\
                 '------------\n'+\
                 '------------\n'+\
                 bin_headline + '\n'+ process_idx_headline + '\n' + process_headline + '\n' + rates_headline + '\n'+\
                 '------------\n'
            for syst_line in systematic_lines:
                T1 += syst_line + '\n'
                 
            
            datacard_name = os.path.join(storage_dir, cardName +'.txt')
            open(datacard_name, 'wt').write(T1)

print(len(NULL_OBS_REGS))
print(NULL_OBS_REGS)

#str(Hists[numyear][10][0][numreg][0].Integral())
#'observation'.ljust(35) + str(sum_of_bkgs[nameyear][namereg]) +'\n'+\ 
